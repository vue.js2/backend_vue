const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: String,
  age: Number,
  gender: String,
  salary: Number
})

module.exports = mongoose.model('User', userSchema)
