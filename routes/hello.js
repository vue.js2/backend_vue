const express = require('express')
const router = express.Router()

router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello Deaw ', params })
})

module.exports = router
